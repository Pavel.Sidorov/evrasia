var imageViewSwiper = new Swiper('.js-indexSwiper .swiper-container', {
	loop: true,
    autoplayDisableOnInteraction: false,
    autoplayStopOnLast: false,
    autoplay: 5000,
    speed: 700,
    effect: 'fade',
    nextButton: '.js-indexSwiper .wrap-main__button-next',
    prevButton: '.js-indexSwiper .wrap-main__button-prev'
});