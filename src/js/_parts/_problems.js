$(document).on('click', '.problems-list__label', function(){
	$(this).addClass('is-selected');
});

$(document).on('click', '.js-problemsTooltipClose', function(event){
	event.stopPropagation();
	$('.problems-list__label').removeClass('is-selected');
});

$(document).on('mouseenter', '.js-problemsElemHover', function(){
	var $label = $(this).parents('.problems-list__tooltip').find('.js-problemsLabel span'),
		_thisLabel = $(this).data('label');

	$label.text(_thisLabel);
});

$(document).on('mouseleave', '.problems-list__tooltip-methods', function(){
	var $label = $(this).parents('.problems-list__tooltip').find('.js-problemsLabel'),
		$labelSpan = $label.find('span'),
		labelTitle = $label.data('label');

	$labelSpan.text(labelTitle);
});