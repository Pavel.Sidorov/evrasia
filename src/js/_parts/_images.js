$(window).on('load', function(){
	lowresImages($('[data-x2]'));
});

function lowresImages(elem){
	if(window.devicePixelRatio > 1 && $(window).width() >= 768) {
	    var lowresImages = elem;

	    lowresImages.each(function(i) {
	    	$(this).attr({
	    		'width': $(this).width()
	    	});

			var highres = $(this).attr('data-x2');
			$(this).attr('src', highres);
	    });
	}
}