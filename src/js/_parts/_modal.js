$(document)
	.on('click', '.js-modalLink', function(event){
		var modal = $(this).data('modal');
		modalOpen(modal);
		return false;
	})
	.on('click', '.js-modalClose', function(event){
		modalClose();
		return false;
	}).
	on('click', function(event){
		var target = $('.modal__box');

		if($(target).has(event.target).length === 0) {
			modalClose();
		}
	});

function modalOpen(modal){
	$('' + modal + '').addClass('is-open');
}

function modalClose(){
	$('.modal').removeClass('is-open');
	$('body').removeClass('modal-is-open');
}


$(document).on('successModalOpen', function(){
	modalOpen('#modalSuccess');
});

// $(document).trigger('successModalOpen');