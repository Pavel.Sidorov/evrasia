$(document).on('click', '.js-scrollLink', function(){
	var scrollOffset = $('' + $(this).data('scroll') + '').offset().top,
		scrollPadding = $(window).width() <= 767 ? -50 : 0;

	$('body, html').animate({
		scrollTop: scrollOffset + scrollPadding
	}, 600);

	return false;
});

$('.js-scrollNav').onePageNav({
    currentClass: 'is-active',
    changeHash: false,
    offsetNum: $('.js-scrollNav').data('offset') || $(window).width() <= 767 ? -50 : 0,
    scrollSpeed: 750,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {

    },
    end: function() {

    },
    scrollChange: function($currentListItem) {
    
    }
});
