$(document).ready(function(){

	$(function(){
		var $body = $('body'),
			$menu = $('.menu');
	
		$(document)
			.on('click', '.js-burgerMenu', function(){
				var _this = $(this);
	
				if(_this.hasClass('is-active')){
					_this.removeClass('is-active');
					$menu.hide();
					$body.removeClass('is-menu-open is-form-open');
	
					if($(window).width() <= 767){
						$('.header--inner .logo__icon').removeAttr('style');
					}
				}
				else {
					_this.addClass('is-active');
					$menu.show();
					setTimeout(function(){
						$body.addClass('is-menu-open');
	
						if($(window).width() <= 767 && $('.header--inner').length > 0){
							$(document).trigger('logoAnimateTrigger');
						}
					});
				}
			})
			.on('mouseenter', '.contact-control', function(){
				if($(window).width() >= 1201){
					$(this).width($(this).find('.contact-control__label').outerWidth());
				}
			})
			.on('mouseleave', '.contact-control', function(){
				if($(window).width() >= 1201){
					$(this).width('');
				}
			})
			.on('click', '.js-menuRequestLink', function(){
				$body.addClass('is-form-open');
			})
			.on('click', '.js-menuRequestClose', function(){
				$body.removeClass('is-form-open');
			});
	});

	var imageViewSwiper = new Swiper('.js-indexSwiper .swiper-container', {
		loop: true,
	    autoplayDisableOnInteraction: false,
	    autoplayStopOnLast: false,
	    autoplay: 5000,
	    speed: 700,
	    effect: 'fade',
	    nextButton: '.js-indexSwiper .wrap-main__button-next',
	    prevButton: '.js-indexSwiper .wrap-main__button-prev'
	});

	$(function(){
		$('.js-select').styler({
			singleSelectzIndex: 1,
			selectSmartPositioning: false
		});
	});

	$(function(){
		$(document)
			.on('mouseenter', '[data-hover-id]', function(){
				$('[data-backid="' + $(this).data('hover-id') + '"]')
					.addClass('is-show')
					.siblings()
					.removeClass('is-show');
			})
			.on('mouseleave', '[data-hover-id]', function(){
				$('[data-backid]').removeClass('is-show');
			});
	
		var historySwiper = new Swiper('.js-historyCarouselSwiper .swiper-container', {
		    speed: 500,
		    slidesPerView: 3,
		    nextButton: '.js-historyCarouselSwiper .carousel-button.next',
		    prevButton: '.js-historyCarouselSwiper .carousel-button.prev',
		    breakpoints: {
		    	6000: {
		    		slidesPerView: 3
		    	},
		    	767: {
		    		slidesPerView: 1,
		    		autoHeight: true
		    	}
		    }
		});
	
		$(window).on('load resize', function(){
			if($('.js-asideTextPosition').length > 0){
				$('.js-asideTextPosition').css({
					'top': $('.method__list').offset().top
				});
			}
	
			if($(window).width() >= 768){
				$('.methods-screen__first').css({
					'height': $(window).height() - 130
				});
	
				if($('.methods__modal').length > 0){
					$('.methods__modal').css({
						'top': $('.method__list').offset().top - 155
					});
				}
			}
		});
	
		$(document)
			.on('click', '.js-methodModalLink', function(event){
				event.preventDefault();
				$('body').addClass('is-method-modal-open');
			})
			.on('click', '.js-methodModalClose', function(){
				$('body').removeClass('is-method-modal-open');
			});
	});

	$(window).on('load', function(){
		lowresImages($('[data-x2]'));
	});
	
	function lowresImages(elem){
		if(window.devicePixelRatio > 1 && $(window).width() >= 768) {
		    var lowresImages = elem;
	
		    lowresImages.each(function(i) {
		    	$(this).attr({
		    		'width': $(this).width()
		    	});
	
				var highres = $(this).attr('data-x2');
				$(this).attr('src', highres);
		    });
		}
	}

	/*
	 * Карта
	 */
	
	function initializeMap(){
		var WINDOW_WIDTH = $(window).width(),
			map = $('.js-map'),
			pin = map.data('pin');
	
		map.each(function(){
			var map, 
				loc = $(this).data('location').split(','),
				desktopLoc = WINDOW_WIDTH > 767 ? +loc[1] + 0.09 : +loc[1] + 0.02,
				position = new google.maps.LatLng(loc[0], loc[1]),
				mapOptions = {
			        center: new google.maps.LatLng(loc[0], loc[1]),
			        zoom: 17,
			        mapTypeId: google.maps.MapTypeId.ROADMAP,
			        mapTypeControlOptions: {
			            
			        },
			        disableDefaultUI: true,
			        scrollwheel: false,
			        rotateControl: false,
			        // styles: [{featureType:"all", elementType:"all", stylers:[{saturation:-100}, {gamma:1}]}]
		    	};
	
		    map = new google.maps.Map($(this).get(0), mapOptions);
		    map.setTilt(45);
	
		    var marker = new google.maps.Marker({
		        position: position,
		        map: map,
		        title: '',
		        animation: google.maps.Animation.DROP,
		        icon: {
		            url: pin,
		            scaledSize: new google.maps.Size(70, 87)
		        }
		    });
	
			map.setOptions({draggable: WINDOW_WIDTH > 767});
		});
	}
	
	if($('body').find('.js-map').length){
		google.maps.event.addDomListener(window, 'load', initializeMap);
	}
	
	$(function(){
		var $body = $('body');
	
		$(document).on('click', '.js-contactRequestLink', function(){
			$body.addClass('is-request-open');
		});
	
		$(document).on('click', '.js-contactRequestClose', function(){
			$body.removeClass('is-request-open');
		});
	});

	document.addEventListener('touchstart', function(){}, true);

	$(document).on('click', '.js-scrollLink', function(){
		var scrollOffset = $('' + $(this).data('scroll') + '').offset().top,
			scrollPadding = $(window).width() <= 767 ? -50 : 0;
	
		$('body, html').animate({
			scrollTop: scrollOffset + scrollPadding
		}, 600);
	
		return false;
	});
	
	$('.js-scrollNav').onePageNav({
	    currentClass: 'is-active',
	    changeHash: false,
	    offsetNum: $('.js-scrollNav').data('offset') || $(window).width() <= 767 ? -50 : 0,
	    scrollSpeed: 750,
	    scrollThreshold: 0.5,
	    filter: ':not(.external)',
	    easing: 'swing',
	    begin: function() {
	
	    },
	    end: function() {
	
	    },
	    scrollChange: function($currentListItem) {
	    
	    }
	});

	$(function(){
	
		var aboutSwiper = new Swiper('.js-aboutPhotosSwiper .swiper-container', {
		    speed: 500,
		    loop: true,
		    slidesPerView: 1,
		    nextButton: '.js-aboutPhotosSwiper .carousel-button.next',
		    prevButton: '.js-aboutPhotosSwiper .carousel-button.prev',
		    pagination: '.js-aboutPhotosSwiper .swiper-pagination',
		    onSlideChangeStart: function(swiper){
		    	var active = swiper.realIndex + 1;
	
		    	$('.swiper-count__active').text(active);
		    }
		});
	
		$(window).on('load resize', function(){
			if($('.about__screen').length > 0){
				aboutHeight();
			}
		});
	
		function aboutHeight(){
			if($(window).width() <= 767){
				$('.about__screen').css({
					'min-height': $(window).height() - $('.about__screen').offset().top 
				});
			}
		}
	
		if($('.about__screen').length > 0){
			aboutHeight();
		}
	});

	$(function(){
		var logoShift = 0;
		var isMobile = false;
	
		function animateLogo() {
			logoShift = 0;
			isMobile = window.innerWidth < 768;
			setTimeout(function () {
				logoAnimStep();
			}, 500)
		}
	
		function logoAnimStep() {
			var logo = $('.logo__icon');
	
			logo.css({'opacity': '1'});
	
			setTimeout(function () {
				logoShift += isMobile ? 27 : 35; // width
				if (logoShift >= (isMobile ? 756 : 980)) {
					return;
				}
				logo.css('background-position', '-' + logoShift + 'px 0');
				logoAnimStep();
			}, 40);
		}
	
		$(window).on('load', function(){
			if($('body').hasClass('animate')){
				setTimeout(function(){
					animateLogo();
				}, 800);
			}
			else {
				animateLogo();	
			}
	
			$('.animate').removeClass('animate');
		});
	
		$(document).on('logoAnimateTrigger', function(){
			animateLogo();
		});
	});

	$(document).on('click', '.problems-list__label', function(){
		$(this).addClass('is-selected');
	});
	
	$(document).on('click', '.js-problemsTooltipClose', function(event){
		event.stopPropagation();
		$('.problems-list__label').removeClass('is-selected');
	});
	
	$(document).on('mouseenter', '.js-problemsElemHover', function(){
		var $label = $(this).parents('.problems-list__tooltip').find('.js-problemsLabel span'),
			_thisLabel = $(this).data('label');
	
		$label.text(_thisLabel);
	});
	
	$(document).on('mouseleave', '.problems-list__tooltip-methods', function(){
		var $label = $(this).parents('.problems-list__tooltip').find('.js-problemsLabel'),
			$labelSpan = $label.find('span'),
			labelTitle = $label.data('label');
	
		$labelSpan.text(labelTitle);
	});

	$(function(){
		var $specialistGrid = $('.js-specialistGrid').isotope({
			itemSelector: '.specialist-col',
			layoutMode: 'fitRows'
		});
	
		$(document).on('click', '.js-specialistNav a', function(){
			$(this).parent().addClass('is-active').siblings().removeClass('is-active');
			specialistFilter($(this).data('filter'));
		});
	
		$(document).on('change', 'select.js-specialistSelect', function(){
			specialistFilter($(this).val());
		});
	
		function specialistFilter(val){
			var selected = val;
			
			$specialistGrid.isotope({
				filter: selected
			});
		}
	
	});

	$(document)
		.on('click', '.js-modalLink', function(event){
			var modal = $(this).data('modal');
			modalOpen(modal);
			return false;
		})
		.on('click', '.js-modalClose', function(event){
			modalClose();
			return false;
		}).
		on('click', function(event){
			var target = $('.modal__box');
	
			if($(target).has(event.target).length === 0) {
				modalClose();
			}
		});
	
	function modalOpen(modal){
		$('' + modal + '').addClass('is-open');
	}
	
	function modalClose(){
		$('.modal').removeClass('is-open');
		$('body').removeClass('modal-is-open');
	}
	
	
	$(document).on('successModalOpen', function(){
		modalOpen('#modalSuccess');
	});
	
	// $(document).trigger('successModalOpen');

	$('[data-fancybox]').fancybox({
	    clickOutside: 'close',
	    toolbar : true,
	    buttons : [
	        'close'
	    ],
	    mobile: {
	        clickSlide : function( current, event ) {
	            return current.type === 'image' ? 'close' : false;
	        }
	    }
	});

});